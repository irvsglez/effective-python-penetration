import requests
from bs4 import BeautifulSoup
import urlparse
import sys

response = requests.get('http://www.freeimages.co.uk/galleries/food/breakfast/index.htm')
parse = BeautifulSoup(response.text)

print(parse)

image_tags = parse.find_all('img')

print(image_tags)

images = [url.get('src') for url in image_tags]
if not images:
  sys.exit('Found no images')

images = [urlparse.urljoin(response.url, url) for url in images]
print 'Found %s images' % len(images)

for url in images:
  r = requests.get(url)
  f = open('/Users/israel.rivas/repos/effective-python-penetration/scripts/%s' % url.split('/')[-1], 'w')
  f.write(r.content)
  f.close()
  print 'Downloaded %s' % url
